﻿using System;
using System.Windows.Forms;

namespace VideoScreensaver
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length > 0)
            {
                var firstArgument = args[0].ToLower().Trim();
                string secondArgument = null;

                // Handle cases where arguments are separated by colon.
                // Examples: /c:1234567 or /P:1234567
                if (firstArgument.Length > 2)
                {
                    secondArgument = firstArgument.Substring(3).Trim();
                    firstArgument = firstArgument.Substring(0, 2);
                }
                else if (args.Length > 1)
                    secondArgument = args[1];

				switch (firstArgument)
				{
					case "/c":
						LoadSettings();
						break;
					case "/p":
						if (secondArgument == null)
						{
							MessageBox.Show("Sorry, but the expected window handle was not provided.",
								"ScreenSaver", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
							return;
						}

						ScreensaverSettings.Instance.Load();
						VideoSynchronizer.Instance.FindMovies(ScreensaverSettings.Instance.MoviePath);

						var previewWndHandle = new IntPtr(long.Parse(secondArgument));
						Application.Run(new ScreenSaverForm(previewWndHandle));
						break;
					case "/s":
						ShowScreenSaver();
						Application.Run();
						break;
					default:
						MessageBox.Show("Sorry, but the command line argument \"" + firstArgument +
			  "\" is not valid.", "ScreenSaver",
			  MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
						break;
				}
			}
            else    // No arguments - treat like /c
            {
                LoadSettings();
            }            
        }

        static void ShowScreenSaver()
        {
            ScreensaverSettings.Instance.Load();
            VideoSynchronizer.Instance.FindMovies(ScreensaverSettings.Instance.MoviePath);
            int nbScreens = Screen.AllScreens.Length;
            for (int i = 0; i < nbScreens; ++i)
            {
                var screensaver = new ScreenSaverForm(i);
                VideoSynchronizer.Instance.RegisterScreensaver(screensaver);
                screensaver.Show();
            }
            VideoSynchronizer.Instance.StartScreensavers();
        }

        static void LoadSettings()
        {
            ScreensaverSettings.Instance.Load();
            var config = new ConfigForm(ScreensaverSettings.Instance);
            if (config.ShowDialog() == DialogResult.OK)
                ScreensaverSettings.Instance.Save();

        }
    }
}
