﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Controls;

namespace VideoScreensaver
{
	public partial class ScreenSaverForm : Form
    {
        private System.Drawing.Point mouseLocation;
        private bool previewMode;
        private int screenIndex;
        private ScreensaverSettings settings { get { return ScreensaverSettings.Instance; } }
        System.Windows.RoutedEventHandler videoEnded;
        private MediaElement mediaElement;

        public event EventHandler ReadyToPlay;

        public ScreenSaverForm() : this(0) {}

        public ScreenSaverForm(int screenIndex)
        {
            InitializeComponent();
            videoEnded = new System.Windows.RoutedEventHandler(mediaElement_MediaEnded);
            this.screenIndex = screenIndex;

            if (screenIndex == 0 || settings.PlayOnAllScreens)
            {
                mediaElement = new MediaElement();
                mediaElement.MouseMove += mediaElement_MouseMove;
                mediaElement.MouseDown += mediaElement_MouseDown;
                mediaElement.LoadedBehavior = MediaState.Manual;
                elementHost1.Child = mediaElement;
            }
           
        }

        public ScreenSaverForm(IntPtr PreviewWndHandle)
            : this(0)
        {
            // Set the preview window as the parent of this window
            NativeMethods.SetParent(this.Handle, PreviewWndHandle);

            // Make this a child window so it will close when the parent dialog closes
            // GWL_STYLE = -16, WS_CHILD = 0x40000000
            NativeMethods.SetWindowLong(this.Handle, -16, new IntPtr(NativeMethods.GetWindowLong(this.Handle, -16) | 0x40000000));

            // Place our window inside the parent
            Rectangle ParentRect;
            NativeMethods.GetClientRect(PreviewWndHandle, out ParentRect);
            Size = ParentRect.Size;
            Location = new System.Drawing.Point(0, 0);

            previewMode = true;
            LoadVideo();
            Start();
        }

        private delegate void DoSomething();
        public void Start()
        {
            if (mediaElement != null)
                BeginInvoke(new DoSomething(ResetPosition));
        }

        private void ResetPosition()
        {
            if (mediaElement != null)
                mediaElement.Play();
        }

        public void LoadVideo()
        {
            try
            {
                if (!String.IsNullOrEmpty(VideoSynchronizer.Instance.CurrentMovie))
                {
                    mediaElement.Stop();
                    mediaElement.Source = new System.Uri(VideoSynchronizer.Instance.CurrentMovie);
                    mediaElement.MediaEnded += videoEnded;

                    /*if (screenIndex > 0)
                    {
                        HwndSource hwndSource = PresentationSource.FromVisual(mediaElement) as HwndSource;
                        HwndTarget hwndTarget = hwndSource.CompositionTarget;
                        hwndTarget.RenderMode = RenderMode.SoftwareOnly;
                    }*/

                    mediaElement.Volume = screenIndex > 0 ? 0 : (settings.PlayAudio ? 1 : 0);
                    //mediaElement.Play();
                    if(ReadyToPlay != null)
                        ReadyToPlay(this, null);
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Unable to play " + VideoSynchronizer.Instance.CurrentMovie);
                Console.WriteLine(e.StackTrace);
                if (screenIndex == 0)
                {
                    VideoSynchronizer.Instance.StartScreensavers();
                }
            }
        }

        void mediaElement_MediaEnded(object sender, System.Windows.RoutedEventArgs e)
        {
            mediaElement.MediaEnded -= videoEnded;
            if (screenIndex == 0)
            {
                VideoSynchronizer.Instance.StartScreensavers();
            }
        }

        private void ScreenSaverForm_Load(object sender, EventArgs e)
        {
            if (!previewMode)
            {
                Bounds = Screen.AllScreens[screenIndex].Bounds;
                Cursor.Hide();
                TopMost = true;
            }
        }

        void mediaElement_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            KillApp();
        }

        void mediaElement_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            System.Drawing.Point newPos = System.Windows.Forms.Cursor.Position;
            if (!mouseLocation.IsEmpty)
            {
                var delta = Math.Max(Math.Abs(newPos.X - mouseLocation.X), Math.Abs(newPos.Y - mouseLocation.Y));
                if (delta > 5)
                {
                    KillApp();
                }
            }
            mouseLocation = newPos;
        }

        private void KillApp()
        {
            if (!previewMode)
                System.Windows.Forms.Application.Exit();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            HandleKey(keyData);
            return base.ProcessCmdKey(ref msg, keyData);
        }

        public void HandleKey(Keys keyData)
        {
            Console.WriteLine(keyData);
            switch (keyData)
            {
                case Keys.N:
                    VideoSynchronizer.Instance.StartScreensavers();
                    break;
                default:
                    KillApp();
                    break;
            }
        }   
    }
}
