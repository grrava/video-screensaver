﻿using System;
using Microsoft.Win32;
using System.ComponentModel;

namespace VideoScreensaver
{

	public class ScreensaverSettings
    {
        private static readonly ScreensaverSettings instance = new ScreensaverSettings();
        private ScreensaverSettings() { }
        public static ScreensaverSettings Instance { get { return instance; } }

        private static readonly string registryRoot = "HKEY_CURRENT_USER\\Software\\Ava\\VideoScreensaver";
        public bool PlayAudio { get; set; }
        public bool PlayOnAllScreens { get; set; }

        [EditorAttribute(typeof(System.Windows.Forms.Design.FolderNameEditor), typeof(System.Drawing.Design.UITypeEditor))]
        public string MoviePath { get; set; }

        public void Save()
        {
            Registry.SetValue(registryRoot, "PlayAudio", PlayAudio, RegistryValueKind.String);
            Registry.SetValue(registryRoot, "MoviePath", MoviePath, RegistryValueKind.String);
            Registry.SetValue(registryRoot, "PlayOnAllScreens", PlayOnAllScreens, RegistryValueKind.String);
        }

        public void Load()
        {
            var playOnAllScreens = Registry.GetValue(registryRoot, "PlayOnAllScreens", PlayOnAllScreens) as string;
            if (!String.IsNullOrEmpty(playOnAllScreens))
            {
                PlayOnAllScreens = Boolean.Parse(playOnAllScreens);
            }
            var playAudio = Registry.GetValue(registryRoot, "PlayAudio", PlayAudio) as string;
            if (!String.IsNullOrEmpty(playAudio))
            {
                PlayAudio = Boolean.Parse(playAudio);
            }
            var moviePath = Registry.GetValue(registryRoot, "MoviePath", MoviePath) as string;
            if (!String.IsNullOrEmpty(moviePath))
            {
                MoviePath = moviePath;
            }
        }
    }
}
