﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.IO;

namespace VideoScreensaver
{
	public class VideoSynchronizer
    {
        private static readonly VideoSynchronizer instance = new VideoSynchronizer();
        private VideoSynchronizer() { }
        public static VideoSynchronizer Instance { get { return instance; } }

        DateTime nextStart;
		readonly List<ScreenSaverForm> screensavers = new List<ScreenSaverForm>();
		readonly List<BackgroundWorker> workers = new List<BackgroundWorker>();
		List<string> movies = new List<string>();
        List<string> history = new List<string>();
        int nbReadyToPlay;

        public string CurrentMovie 
        { 
            get 
            { 
                if(history.Count  > 0)
                    return history[history.Count - 1];
                return String.Empty;
            } 
        }

        public void RegisterScreensaver(ScreenSaverForm screensaver)
        {
            screensavers.Add(screensaver);
            screensaver.ReadyToPlay += screensaver_ReadyToPlay;

            var bw = new BackgroundWorker();
            bw.DoWork += bw_DoWork;
            workers.Add(bw);
        }

        public void FindMovies(string path)
        {
            if (!String.IsNullOrEmpty(path))
            {
                movies.AddRange(Directory.GetFiles(path, "*.mov"));
                movies.AddRange(Directory.GetFiles(path, "*.avi"));
                movies.AddRange(Directory.GetFiles(path, "*.wmv"));
                movies.AddRange(Directory.GetFiles(path, "*.mp4"));
                if (movies.Count > 0)
                    history.Add(movies[0]);
            }
        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            while (DateTime.Now.Ticks < nextStart.Ticks)
            {
                Thread.Sleep(10);
            }
            ScreenSaverForm screensaver = screensavers[(int)e.Argument];
                screensaver.Start();
        }

        public void StartScreensavers()
        {
            if (movies.Count > 0)
            {
                var rand = new Random();
                history.Add(movies[rand.Next(movies.Count)]);
                foreach (ScreenSaverForm screensaver in screensavers)
                {
                    screensaver.LoadVideo();
                }
            }
        }

        void screensaver_ReadyToPlay(object sender, EventArgs e)
        {
            nbReadyToPlay++;
            if (nbReadyToPlay == screensavers.Count || !ScreensaverSettings.Instance.PlayOnAllScreens)
            {
                nextStart = DateTime.Now.AddSeconds(1);
                for (int i = 0; i < workers.Count; ++i)
                    workers[i].RunWorkerAsync(i);
                nbReadyToPlay = 0;
            }
          
        }
    }
}
